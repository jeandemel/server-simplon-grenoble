# server-simplon-grenoble

Instructions pour mettre vos sites Symfony en ligne sur le serveur simplon-grenoble

1. Faire un export de votre base de donnée en local (avec workbench/adminer/phpmyadmin/la console symfony)
2. Aller sur https://mysql.simplon-grenoble.fr et vous connecter sans base de données avec comme identifiant et mot de passe : première lettre de votre prénom et nom complet. Exemples: Bernard Friot > bfriot - Marie Cecile Du Chateau > mduchateau
3. Créer une base de donnée (celle ci doit commencer par `username_` où username est votre identifiant)
4. Importer la base de données que vous avez exportée
5. Vous connecter en ftp sur votre domaine et uploader le dossier Symfony
6. Attendre à peu près 1000 ans que le transfert des fichiers soit terminé
7. Ajouter le fichier .htaccess de ce dépôt dans le dossier public de votre projet
8. Modifier le fichier .env.local pour mettre les informations de connexion du serveur (exemple: `DATABASE_URL=mysql://username:username@localhost:3306/username_bloup`)
9. Accéder au site via une une url genre : https://username.simplon-grenoble.fr/mon-projet/public (ouais, on est obligé de mettre le public à moins de changer la configuration du serveur)

Si ya des soucis, ouvrez une issue sur ce dépôt, non seulement ça me faciletera la tâche mais en plus ça gardera une trace pour celleux qu'auraient un problème similaire

